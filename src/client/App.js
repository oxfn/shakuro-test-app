import React, { Component, Fragment } from 'react';
import { HashRouter, Route, Switch } from 'react-router-dom';
import { hot } from 'react-hot-loader/root';
import PropTypes from 'prop-types';
import CircularProgress from '@material-ui/core/CircularProgress';
import Snackbar from '@material-ui/core/Snackbar';
import axios from 'axios';

import withStore from 'common/withStore';
import { getStatus, setError, clearError } from 'store/system/actions';
import { isInit, errorMessage } from 'store/system/selectors';
import Header from './components/Header';
import MainPage from './components/pages/main';
import OpDetailsPage from './components/pages/details';
import LoginPage from './components/pages/login';
import NotFoundPage from './components/pages/notfound';

class App extends Component {
  static propTypes = {
    getStatus: PropTypes.func.isRequired,
    isInit: PropTypes.bool.isRequired,
    clearError: PropTypes.func.isRequired,
    setError: PropTypes.func.isRequired,
    errorMessage: PropTypes.any,
  }

  static defaultProps = {
    errorMessage: null,
  }

  errorTimeout = null;

  componentDidMount() {
    axios.interceptors.response.use((res) => {
      const { data } = res;
      let errorIsSet = false;
      if (res.status === 200) {
        if (data.success === false && data.message) {
          this.props.setError(data.message);
          errorIsSet = true;
        }
      } else {
        this.props.setError('Unexpected server error');
        errorIsSet = true;
      }
      if (errorIsSet) {
        if (this.errorTimeout) {
          clearTimeout(this.errorTimeout);
        }
        this.errorTimeout = setTimeout(() => {
          this.props.clearError();
          this.errorTimeout = null;
        }, 1000);
      }
      return res;
    });
    this.props.getStatus();
  }

  render() {
    return (
      <HashRouter>
        {this.props.isInit ? (
          <div style={{ width: '40px', margin: '240px auto' }}>
            <CircularProgress />
          </div>
        ) : (
          <Fragment>
            <Header />
            <Switch>
              <Route path="/login" component={LoginPage} />
              <Route path="/ops/:name" component={OpDetailsPage} />
              <Route path="/" exact component={MainPage} />
              <Route path="*" component={NotFoundPage} />
            </Switch>
            <Snackbar
              anchorOrigin={{
                horizontal: 'right',
                vertical: 'bottom',
              }}
              color="red"
              open={!!this.props.errorMessage}
              message={this.props.errorMessage}
            />
          </Fragment>
        )
      }
      </HashRouter>
    );
  }
}

export default hot(
  withStore({
    isInit,
    errorMessage,
  }, {
    getStatus,
    setError,
    clearError,
  })(App),
);
