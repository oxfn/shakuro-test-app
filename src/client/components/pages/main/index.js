import React from 'react';
import PropTypes from 'prop-types';
import { withRouter } from 'react-router-dom';
import {
  Card,
  CardActionArea,
  CardContent,
  CardHeader,
  Typography,
} from '@material-ui/core';
import withStyles from '@material-ui/core/styles/withStyles';
import withAuth from 'common/withAuth';
import withStore from 'common/withStore';
import OpIcon from 'common/OpIcon';
import { getOps } from 'store/ops/actions';
import { operators } from 'store/ops/selectors';

const styles = theme => ({
  root: {
    padding: '48px',
  },
  container: {
    display: 'flex',
  },
  item: {
    flexGrow: 1,
    margin: theme.spacing.unit,
    minWidth: '200px',
  },
  itemAction: {
    padding: `${theme.spacing.unit}px ${theme.spacing.unit * 2}px`,
  },
  dataField: {
    minHeight: '24px',
    marginBottom: '20px',
  },
});

class MainPage extends React.Component {
  static propTypes = {
    classes: PropTypes.object.isRequired,
    getOps: PropTypes.func.isRequired,
    history: PropTypes.object.isRequired,
    isLoggedIn: PropTypes.bool.isRequired,
    operators: PropTypes.array.isRequired,
  };

  componentDidMount() {
    if (this.props.isLoggedIn) {
      this.props.getOps();
    }
  }

  handleDetails = op => () => {
    this.props.history.push(`/ops/${op.name}`);
  }

  render() {
    const { classes } = this.props;
    return (
      <div className={classes.root}>
        <div className={classes.container}>
          {this.props.operators && this.props.operators.length
            ? Object.values(this.props.operators).map(op => (
              <Card key={op.name} className={classes.item}>
                <CardActionArea className={classes.itemAction} onClick={this.handleDetails(op)}>
                  <CardHeader
                    avatar={<OpIcon name={op.name} />}
                    title={op.displayName}
                    titleTypographyProps={{ variant: 'title' }}
                  />
                  <CardContent>
                    <div className={classes.dataField}>
                      {op.number
                        ? <Typography variant="subtitle1">{op.number}</Typography>
                        : ' '}
                    </div>
                    <div className={classes.dataField}>
                      {op.balance
                        ? (<Typography variant="headline">{`$ ${op.balance}`}</Typography>
                        ) : ' '}
                    </div>
                  </CardContent>
                </CardActionArea>
              </Card>
            ))
            : null
          }
        </div>
      </div>
    );
  }
}

export default withStyles(styles)(
  withStore({ operators }, { getOps })(
    withRouter(withAuth(MainPage)),
  ),
);
