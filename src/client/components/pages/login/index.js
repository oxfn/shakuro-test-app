import React from 'react';
import PropTypes from 'prop-types';
import { withRouter } from 'react-router-dom';
import Button from '@material-ui/core/Button';
import Paper from '@material-ui/core/Paper';
import TextField from '@material-ui/core/TextField';
import Typography from '@material-ui/core/Typography';
import withStyles from '@material-ui/core/styles/withStyles';

import withStore from 'common/withStore';
import { login } from 'store/system/actions';
import { isLoggedIn } from 'store/system/selectors';

const styles = theme => ({
  root: {
    width: '300px',
    margin: '100px auto',
    padding: `${theme.spacing.unit * 6}px ${theme.spacing.unit * 6}px`,
  },
  center: {
    textAlign: 'center',
  },
  loginForm: {
  },
  textInput: {
    marginTop: theme.spacing.unit * 2,
    width: '100%',
  },
  submitButton: {
    marginTop: theme.spacing.unit * 3,
  },
});

class LoginPage extends React.Component {
  static propTypes = {
    classes: PropTypes.object.isRequired,
    isLoggedIn: PropTypes.bool.isRequired,
    login: PropTypes.func.isRequired,
    history: PropTypes.object.isRequired,
  }

  state = {
    username: '',
    password: '',
    error: false,
  }

  isLoginDisabled = () => !this.state.username || !this.state.password;

  handleChange = name => (event) => {
    this.setState({
      [name]: event.target.value,
      error: false,
    });
  }

  handleSubmit = async (event) => {
    event.persist();
    event.preventDefault();
    if (this.isLoginDisabled()) {
      return;
    }
    await this.props.login(this.state.username, this.state.password);
    if (this.props.isLoggedIn) {
      this.props.history.push('/');
    } else {
      this.setState({
        error: true,
      });
    }
  }

  render() {
    const { classes } = this.props;
    return (
      <Paper className={classes.root}>
        <Typography variant="h2" align="center">Welcome</Typography>
        <form className={classes.loginForm} onSubmit={this.handleSubmit}>
          <div className={classes.center}>
            <TextField
              className={classes.textInput}
              error={this.state.error}
              label="User name"
              name="username"
              onChange={this.handleChange('username')}
              value={this.state.username}
            />
            <TextField
              className={classes.textInput}
              error={this.state.error}
              label="Password"
              name="password"
              onChange={this.handleChange('password')}
              type="password"
              value={this.state.password}
            />
          </div>
          <Typography align="right">
            <Button
              className={classes.submitButton}
              color="primary"
              disabled={this.isLoginDisabled()}
              onClick={this.handleSubmit}
              type="submit"
              variant="outlined"
            >
              Login
            </Button>
          </Typography>
        </form>
      </Paper>
    );
  }
}

export default withStyles(styles)(
  withStore({ isLoggedIn }, { login })(
    withRouter(LoginPage),
  ),
);
