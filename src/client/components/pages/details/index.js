import React from 'react';
import PropTypes from 'prop-types';
import {
  Button,
  Paper,
  withStyles,
} from '@material-ui/core';
import { phoneNumber } from 'shared/libs/validation';
import withAuth from 'common/withAuth';
import withStore from 'common/withStore';
import PhoneInput from 'common/inputs/PhoneInput';
import AmountInput from 'common/inputs/AmountInput';
import { getOps, updateOp } from 'store/ops/actions';
import { operator, operators } from 'store/ops/selectors';

const ERR_BALANCE = 'Balance should be in range $1 - 1000';
const ERR_PHONE = 'Phone has invalid format';

const styles = theme => ({
  root: {
    margin: '120px auto',
    padding: theme.spacing.unit * 4,
    width: '360px',
  },
  formField: {
    marginBottom: theme.spacing.unit * 2,
    width: '100%',
  },
  button: {
    marginRight: theme.spacing.unit * 2,
  },
});

const validators = {
  balance: value => (!value || value < 1 || value > 1000) && ERR_BALANCE,
  number: value => !phoneNumber(value) && ERR_PHONE,
};

class OpDetailsPage extends React.Component {
  static propTypes = {
    classes: PropTypes.object.isRequired,
    getOps: PropTypes.func.isRequired,
    history: PropTypes.object.isRequired,
    match: PropTypes.object.isRequired,
    operator: PropTypes.func.isRequired,
    operators: PropTypes.array.isRequired,
    updateOp: PropTypes.func.isRequired,
  }

  state = {
    name: null,
    balance: 0,
    errors: {
      balance: null,
      number: null,
    },
    number: '',
  }

  async componentDidMount() {
    if (!this.props.operators.length) {
      await this.props.getOps();
    }
    const { name } = this.props.match.params;
    const { balance, number } = this.props.operator(name);
    this.setState({ name, balance, number });
  }

  isSubmitDisabled = () => Object.values(this.state.errors).some(e => e);

  handleChange = name => (event) => {
    const { value } = event.target;
    this.setState(state => ({
      [name]: value,
      errors: {
        ...state.errors,
        [name]: validators[name](value),
      },
    }));
  }

  handleSubmit = async (event) => {
    event.persist();
    event.preventDefault();
    if (this.isSubmitDisabled()) {
      return;
    }
    await this.props.updateOp(this.state);
    this.props.history.push('/');
  }

  handleCancel = () => {
    this.props.history.push('/');
  }

  render() {
    const { classes } = this.props;
    return (
      <Paper className={classes.root}>
        <form>
          <PhoneInput
            id="number_input"
            className={classes.formField}
            error={!!this.state.errors.number}
            name="number"
            label="Number"
            onChange={this.handleChange('number')}
            value={this.state.number}
          />
          <AmountInput
            id="balance_input"
            className={classes.formField}
            error={!!this.state.errors.balance}
            name="balance"
            label="Balance"
            onChange={this.handleChange('balance')}
            value={this.state.balance}
          />
          <div>
            <Button
              className={classes.button}
              color="primary"
              disabled={this.isSubmitDisabled()}
              onClick={this.handleSubmit}
              type="submit"
              variant="outlined"
            >
              Save
            </Button>
            <Button
              className={classes.button}
              color="primary"
              onClick={this.handleCancel}
              variant="outlined"
            >
              Cancel
            </Button>
          </div>
        </form>
      </Paper>
    );
  }
}

export default withStyles(styles)(
  withStore({ operator, operators }, { getOps, updateOp })(
    withAuth(OpDetailsPage),
  ),
);
