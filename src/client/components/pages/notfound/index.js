import React from 'react';
import PropTypes from 'prop-types';
import Paper from '@material-ui/core/Paper';
import Typography from '@material-ui/core/Typography';
import withStyles from '@material-ui/core/styles/withStyles';

const styles = theme => ({
  root: {
    margin: '120px auto',
    padding: `${theme.spacing.unit * 6}px ${theme.spacing.unit * 3}px`,
    width: '480px',
  },
});

const NotFoundPage = (props) => {
  const { classes, match: { url } } = props;
  return (
    <Paper className={classes.root}>
      <Typography variant="h2">404</Typography>
      <Typography variant="headline">
        Sorry, nothing found at
        <span>{url}</span>
      </Typography>
    </Paper>
  );
};

NotFoundPage.propTypes = {
  classes: PropTypes.object.isRequired,
  match: PropTypes.object.isRequired,
};

export default withStyles(styles)(NotFoundPage);
