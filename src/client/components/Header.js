import React from 'react';
import { Link } from 'react-router-dom';
import PropTypes from 'prop-types';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import withStyles from '@material-ui/core/styles/withStyles';

import LoginControl from './LoginControl';

const styles = {
  grow: {
    flexGrow: 1,
  },
  appLink: {
    color: 'inherit',
    textDecoration: 'none',
  },
};

const Header = (props) => {
  const { classes } = props;
  return (
    <AppBar position="static">
      <Toolbar>
        <Typography variant="h6" color="inherit">
          <Link className={classes.appLink} to="/">Super Useful App</Link>
        </Typography>
        <span className={classes.grow} />
        <LoginControl />
      </Toolbar>
    </AppBar>
  );
};

Header.propTypes = {
  classes: PropTypes.object.isRequired,
};
export default withStyles(styles)(Header);
