import React from 'react';
import PropTypes from 'prop-types';
import { withRouter } from 'react-router-dom';
import Button from '@material-ui/core/Button';
import withStyles from '@material-ui/core/styles/withStyles';

import withStore from 'common/withStore';
import { logout } from 'store/system/actions';
import { isLoggedIn } from 'store/system/selectors';

const styles = {
};

class LoginControl extends React.Component {
  static propTypes = {
    isLoggedIn: PropTypes.bool.isRequired,
    history: PropTypes.object.isRequired,
    logout: PropTypes.func.isRequired,
  }

  handleClick = () => {
    if (this.props.isLoggedIn) {
      this.props.logout();
    } else {
      this.props.history.push('/login');
    }
  }

  render() {
    return (
      <Button variant="outlined" color="inherit" onClick={this.handleClick}>
        {this.props.isLoggedIn ? 'Logout' : 'Login'}
      </Button>
    );
  }
}

export default withStyles(styles)(
  withStore({
    isLoggedIn,
  }, {
    logout,
  })(
    withRouter(LoginControl),
  ),
);
