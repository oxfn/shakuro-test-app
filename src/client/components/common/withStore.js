import React from 'react';
import { connect } from 'react-redux';
import { createStructuredSelector } from 'reselect';
import { bindActionCreators } from 'redux';

const withStore = function withStore(selectors = {}, actionCreators = {}) {
  return (Wrapped) => {
    const mapStateToProps = createStructuredSelector(selectors);
    const mapDispatchToProps = dispatch => bindActionCreators(actionCreators, dispatch);
    return connect(mapStateToProps, mapDispatchToProps)(
      props => <Wrapped {...props} />,
    );
  };
};

export default withStore;
