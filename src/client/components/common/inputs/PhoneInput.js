import React from 'react';
import PropTypes from 'prop-types';
import MaskedInput from 'react-text-mask';
import { FormControl, InputLabel, Input } from '@material-ui/core';

const InputComponent = (props) => {
  const { inputRef, ...rest } = props;
  return (
    <MaskedInput
      {...rest}
      ref={inputRef}
      mask={['+', '7', ' ', '(', /[1-9]/, /\d/, /\d/, ')', ' ', /\d/, /\d/, /\d/, '-', /\d/, /\d/, '-', /\d/, /\d/]}
      placeholderChar={'\u2000'}
      showMask
    />
  );
};

InputComponent.propTypes = {
  inputRef: PropTypes.func.isRequired,
};

const PhoneInput = (props) => {
  const { className, id, label } = props;
  return (
    <FormControl className={className} variant="outlined">
      {label && (<InputLabel shrink focused htmlFor={id}>{label}</InputLabel>)}
      <Input
        {...props}
        placeholder=""
        inputComponent={InputComponent}
      />
    </FormControl>
  );
};

PhoneInput.defaultProps = {
  label: null,
};

PhoneInput.propTypes = {
  id: PropTypes.string.isRequired,
  className: PropTypes.string.isRequired,
  label: PropTypes.string,
};

export default PhoneInput;
