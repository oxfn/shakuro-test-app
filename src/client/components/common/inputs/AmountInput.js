import React from 'react';
import PropTypes from 'prop-types';
import NumberFormat from 'react-number-format';
import { FormControl, InputLabel, Input } from '@material-ui/core';

const InputComponent = (props) => {
  const { inputRef, onChange, ...rest } = props;

  return (
    <NumberFormat
      {...rest}
      getInputRef={inputRef}
      onValueChange={values => onChange({
        target: {
          value: values.value,
        },
      })}
      thousandSeparator
      prefix="$"
    />
  );
};

InputComponent.propTypes = {
  inputRef: PropTypes.func.isRequired,
  onChange: PropTypes.func.isRequired,
};

const AmountInput = (props) => {
  const { className, id, label } = props;
  return (
    <FormControl className={className} variant="outlined">
      {label && (<InputLabel shrink focused htmlFor={id}>{label}</InputLabel>)}
      <Input
        {...props}
        placeholder=""
        inputComponent={InputComponent}
      />
    </FormControl>
  );
};

AmountInput.defaultProps = {
  label: null,
};

AmountInput.propTypes = {
  id: PropTypes.string.isRequired,
  className: PropTypes.string.isRequired,
  label: PropTypes.string,
};

export default AmountInput;
