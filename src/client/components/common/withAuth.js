import React from 'react';

import withStore from 'common/withStore';
import { isLoggedIn } from 'store/system/selectors';

const withAuth = function withAuth(Wrapped) {
  return withStore({ isLoggedIn })(
    class extends React.Component {
      componentDidMount() {
        this.checkAuth();
      }

      componentDidUpdate(prevProps) {
        if (prevProps.isLoggedIn !== this.props.isLoggedIn) {
          this.checkAuth();
        }
      }

      checkAuth = () => {
        if (this.props.match.pathname !== '/login' && !this.props.isLoggedIn) {
          this.props.history.push('/login');
        }
      }

      handleLogin() { } // eslint-disable-line

      handleLogout() { } // eslint-disable-line

      render() {
        return <Wrapped {...this.props} />;
      }
    },
  );
};

export default withAuth;
