import React from 'react';
import PropTypes from 'prop-types';
import withStyles from '@material-ui/core/styles/withStyles';

import Beeline from './images/beeline.png';
import Megafon from './images/megafon.svg';
import Mts from './images/mts.svg';
import Tele2 from './images/tele2.png';

const imageMap = {
  beeline: Beeline,
  megafon: Megafon,
  mts: Mts,
  tele2: Tele2,
};

const styles = {
  icon: {
    height: '48px',
  },
};

const OpIcon = props => (
  <img className={props.classes.icon} src={imageMap[props.name]} alt="" />
);

OpIcon.propTypes = {
  classes: PropTypes.object.isRequired,
  name: PropTypes.string.isRequired,
};

export default withStyles(styles)(OpIcon);
