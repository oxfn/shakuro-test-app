export const isSeeded = state => state.system.seed;

export const isInit = state => state.system.init;

export const isLoggedIn = state => state.system.user.valid;

export const userName = state => state.system.user.name;

export const errorMessage = state => state.system.error;
