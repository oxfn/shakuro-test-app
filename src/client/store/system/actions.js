import axios from 'axios';

export const GET_STATUS = 'GET_STATUS';
export const LOGIN = 'LOGIN';
export const LOGOUT = 'LOGOUT';
export const SET_ERROR = 'SET_ERROR';
export const CLEAR_ERROR = 'CLEAR_ERROR';

export const getStatus = async () => {
  const action = {
    type: GET_STATUS,
  };

  try {
    const { data } = await axios.get('/api/status');
    action.payload = data;
  } catch (err) {
    action.payload = {};
  }

  return action;
};

export const login = async (username, password) => {
  const action = { type: LOGIN };
  try {
    const { data } = await axios.post('/api/login', { username, password });
    action.payload = data;
  } catch (err) {
    action.payload = {};
  }
  return action;
};

export const logout = async () => {
  const action = { type: LOGOUT };
  try {
    const { data } = await axios.post('/api/logout');
    action.payload = data;
  } catch (err) {
    action.payload = {};
  }
  return action;
};

export const setError = message => ({
  type: SET_ERROR,
  payload: {
    error: message,
  },
});

export const clearError = () => ({
  type: CLEAR_ERROR,
  payload: {
    error: null,
  },
});
