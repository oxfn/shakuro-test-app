import {
  GET_STATUS,
  LOGIN,
  LOGOUT,
  SET_ERROR,
  CLEAR_ERROR,
} from './actions';
import transparentReducer from '../transparentReducer';

export default transparentReducer([
  GET_STATUS,
  LOGIN,
  LOGOUT,
  SET_ERROR,
  CLEAR_ERROR,
], {
  init: true,
  error: null,
  user: {
    valid: false,
  },
});
