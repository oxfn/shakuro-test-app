export default (transparentActions = [], defaultState = {}) => (state = defaultState, action) => (
  transparentActions.includes(action.type)
    ? ({ ...state, ...(action.payload.data || action.payload) })
    : state
);
