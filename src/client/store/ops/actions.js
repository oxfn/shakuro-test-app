import axios from 'axios';

export const GET_OPS = 'GET_OPS';
export const GET_BALANCE = 'GET_BALANCE';
export const SET_BALANCE = 'SET_BALANCE';

export const getOps = async () => {
  const action = { type: GET_OPS };
  try {
    const { data } = await axios.get('/api/ops');
    action.payload = data;
  } catch {
    action.payload = [];
  }
  return action;
};

export const getBalance = async (op) => {
  const action = { type: GET_BALANCE };
  try {
    const { data } = await axios.get(`/api/ops/${op}`);
    action.payload = data;
  } catch (err) {
    action.payload = [];
  }
  return action;
};

export const updateOp = async ({ name, number, balance }) => {
  const action = { type: SET_BALANCE };
  try {
    const { data } = await axios.put(`/api/ops/${name}`, { number, balance });
    action.payload = data;
  } catch (err) {
    action.payload = [];
  }
  return action;
};
