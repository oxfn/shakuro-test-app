import { GET_OPS, SET_BALANCE } from './actions';

export default (state = [], action) => {
  switch (action.type) {
    case GET_OPS:
    case SET_BALANCE:
      return action.payload.data || action.payload;
    default:
      return state;
  }
};
