export const operators = state => state.ops;

export const operator = state => name => state.ops.find(op => op.name === name);

export const totalBalance = state => state.ops.reduce((a, x) => a + x.balance, 0);
