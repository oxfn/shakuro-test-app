import { combineReducers, createStore, applyMiddleware } from 'redux';
import reduxPromise from 'redux-promise';

import ops from './ops/reducer';
import system from './system/reducer';

const rootReducer = combineReducers({
  ops,
  system,
});

/**
|--------------------------------------------------
| DISCLAIMER
| Yes, I have read "You might not need Redux"
| This is just a demonstration of concept
|--------------------------------------------------
*/

export default applyMiddleware(reduxPromise)(() => createStore(rootReducer));
