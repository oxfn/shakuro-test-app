module.exports = ({ success = true, data = null, message = '' }) => ({ success, message, data });
