const mongoose = require('mongoose');
const config = require('../config.json');

function init() {
  mongoose.connect(process.env.MONGODB_URI || config.db.connection, { useNewUrlParser: true });
}

function dispose() {
  // stub
}

module.exports = {
  init,
  dispose,
};
