const crypto = require('crypto');
const config = require('../config.json');

function getPasswordHash(password) {
  return crypto
    .createHash('sha1')
    .update(password + config.secret)
    .digest('base64');
}

module.exports = {
  getPasswordHash,
};
