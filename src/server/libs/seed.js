const User = require('../models/user');
const Op = require('../models/op');
const data = require('../seed.json');

module.exports = async function seed() {
  await User.deleteMany({});
  await Op.deleteMany({});

  // Create default user
  data.users.forEach(async (u) => {
    const user = new User({ name: u.name });
    user.setPassword(u.password);
    await user.save();
    data.ops.forEach((op) => {
      Op.create({ ...op, ...data.opsDefaults, userName: user.name });
    });
  });
};
