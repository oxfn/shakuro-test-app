const mongoose = require('mongoose');

const schema = new mongoose.Schema({
  name: String,
  displayName: String,
  number: String,
  balance: Number,
  userName: String,
});

schema.statics.findByUser = function findByUser(userName, opName = null) {
  return this.find({ userName, name: opName });
};

const model = mongoose.model('Op', schema);

module.exports = model;
