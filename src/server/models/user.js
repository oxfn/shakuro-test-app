const mongoose = require('mongoose');
const { getPasswordHash } = require('../libs/crypto');

const schema = new mongoose.Schema({
  name: String,
  password: String,
});

schema.methods.setPassword = function setPassword(password) {
  this.password = getPasswordHash(password);
};

schema.methods.matchPassword = function match(password) {
  return this.password === getPasswordHash(password);
};

const model = mongoose.model('User', schema);

module.exports = model;
