const express = require('express');
const webpack = require('webpack');
const webpackDevMiddleware = require('webpack-dev-middleware');
const webpackHotMiddleware = require('webpack-hot-middleware');
const webpackConfig = require('../../webpack.config')(process.env, { mode: 'development' });
const bootstrap = require('./bootstrap');

const app = express();
const webpackCompiler = webpack(webpackConfig);

app.use(webpackHotMiddleware(webpackCompiler));
app.use(webpackDevMiddleware(webpackCompiler, {
  logLevel: 'info',
  noInfo: true,
  publicPath: webpackConfig.output.publicPath,
}));
bootstrap(app, 8001);
