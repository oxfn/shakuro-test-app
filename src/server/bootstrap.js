const express = require('express');
const expressSession = require('express-session');
const bodyParser = require('body-parser');
const config = require('./config.json');
const db = require('./libs/db');
const resp = require('./libs/resp');
const apiMiddleware = require('./api');

module.exports = (app, port) => {
  const serverUrl = `http://localhost:${port}`;

  // Connect to DB
  db.init();

  // JSON body parser
  app.use(bodyParser.json({
    extended: false,
  }));

  // JSON response
  app.use(express.json());

  // Session middleware
  app.use(expressSession({
    resave: false,
    saveUninitialized: false,
    secret: config.secret,
  }));

  // Response protocol middleware
  // app.use((req, res, next) => {
  //   const baseSend = res.send;
  //   res.send = function send(data = { success: true, message: '', data: null }) {
  //     baseSend(data);
  //   };
  //   next();
  // });

  // API middleware
  app.use('/api', apiMiddleware);

  app.use((err, req, res, next) => {
    if (err) {
      res.send(resp({
        success: false,
        message: err.message,
      }));
    } else {
      next();
    }
  });

  // Redirect middleware
  app.use((req, res) => {
    res.redirect('/');
  });

  console.log(`Starting server at ${serverUrl}`);
  app.listen(port);
  app.on('close', () => {
    db.dispose();
  });
};
