const express = require('express');
const path = require('path');
const bootstrap = require('./bootstrap');

const app = express();

app.use(express.static(path.resolve(__dirname, '../../dist/assets')));

console.log(process.env);

bootstrap(app, process.env.PORT || 80);
