const express = require('express');

const router = express.Router();

router.post('/login', require('./login'));
router.post('/logout', require('./logout'));
router.get('/init', require('./init'));
router.get('/status', require('./status'));
router.use('/ops', require('./ops'));

module.exports = router;
