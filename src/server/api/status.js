const User = require('../models/user');
const resp = require('../libs/resp');

module.exports = async (req, res) => {
  res.send(resp({
    data: {
      init: false,
      seed: await User.estimatedDocumentCount() > 0,
      user: {
        name: req.session.userName,
        valid: !!req.session.userName,
      },
    },
  }));
};
