const seed = require('../libs/seed');
const resp = require('../libs/resp');

module.exports = (req, res) => {
  try {
    seed();
    res.send(resp({}));
  } catch (err) {
    res.send(resp({ success: false, message: err.message }));
  }
};
