const resp = require('../libs/resp');

module.exports = (req, res) => {
  if (req.session.userName) {
    delete req.session.userName;
  }
  res.send(resp({
    data: {
      user: { valid: false },
    },
  }));
};
