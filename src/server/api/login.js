const User = require('../models/user');
const resp = require('../libs/resp');

module.exports = async (req, res) => {
  try {
    const { username, password } = req.body;
    const user = await User.findOne({ name: username });
    const valid = (user && user.matchPassword(password)) || false;
    const name = (user && user.name);
    if (valid) {
      req.session.userName = name;
    }
    res.send(resp({
      success: valid,
      message: valid ? '' : 'Login failed',
      data: {
        user: { name, valid },
      },
    }));
  } catch (err) {
    res.send(resp({ success: false, message: err.message }));
  }
};
