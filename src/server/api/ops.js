const express = require('express');
const assert = require('assert');
const Op = require('../models/op');
const resp = require('../libs/resp');

const router = express.Router();

router.get('/:name?', async (req, res) => {
  // const { name } = req.params;
  const { userName } = req.session;
  assert(userName, 'User is not logged in');
  const data = await Op.find({ userName });
  res.send(resp({ data }));
});

router.put('/:name', async (req, res) => {
  const { userName } = req.session;
  const { name } = req.params;
  const { balance, number } = req.body;
  assert(userName, 'User is not logged in');
  assert(name, 'Operator name is required');
  await Op.updateOne({ userName, name }, { balance, number });
  const data = await Op.find({ userName });
  res.send(resp({ data }));
});

module.exports = router;
