const phoneNumber = value => /^\+\d+ \(\d+\) \d{3}-\d\d-\d\d$/.test(value);

module.exports = {
  phoneNumber,
};
