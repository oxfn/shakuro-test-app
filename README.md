First, I'd like to analyse and describe what does this code do.

```javascript
/**
  * Find latest occurence of a or b chars in the s string
  * @param {string} s - String to search in
  * @param {string} a - First char
  * @param {string} b - Second char
  * @returns {number} Latest index of a or b found in s
  */
function func(s, a, b) {

  // Return -1 if s is empty string
  if (s.match(/^$/)) {
    return -1;
  }

  // [!] Missing check for empty or non-string value - this will cause an error
  //     if s is anything but string.

  // [!] .test method would be more convenient then .match here.

  // [!] Missing type-checking and validation of a and b params
  
  var i = s.length -1; // Set i to last index
  var aIndex =     -1;
  var bIndex =     -1;
  
  // Start loop until a or b chars will be found in s string
  // or 0 char will be reached
  while ((aIndex == -1) && (bIndex == -1) && (i > 0)) {

    // [!] Possible pitfall - skipping 0 char of s

    // [!] Better to use strict comparison (===)

      // Compare char s[i] with a and b
      if (s.substring(i, i +1) == a) {
        aIndex = i;
      }
      if (s.substring(i, i +1) == b) {
        bIndex = i;
      }

      // Increment index
      i = i - 1;
  }
  
  // Return the latest index of a or b (if found any) or -1 if none was found
  if (aIndex != -1) {
      if (bIndex == -1) {
          return aIndex;
      }
      else {
          return Math.max(aIndex, bIndex);
      }
  }
  if (bIndex != -1) {
      return bIndex;
  }
  else {
      return -1;
  }
}
```

Instead of fixing this code I suppose this implementation

```javascript
const assert = (val, message) => {
  if (!val) {
    throw new Error(`Assertion: ${message}`);
  }
}

const assertType = (name, val, type) => {
  assert(typeof val === type, `${name} should be of type ${type}`);
}

const assertLength = (name, val, min, max = null) => {
  assert(val.length >= min, `${name} should be greater then ${min}`);
  assert(!max || val.length <= max, `${name} should be less then ${max}`);
}

/**
  * Find latest occurence of a or b chars in the s string
  * @param {string} s - String to search in
  * @param {string} a - First char
  * @param {string} b - Second char
  * @returns {number} Latest index of a or b found in s
  */
const getLastIndexOf = (s, ...x) => {
  assertType('s', s, 'string');
  assertLength('s', s, 1);
  x.forEach((_, i) => {
    assertType(`param ${i+1}`, _, 'string');
    assertLength(`param ${i+1}`, _, 1, 1);
  });
  return Math.max(...x.map(_ => s.lastIndexOf(_)));
}
```
